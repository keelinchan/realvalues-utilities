#! /usr/local/bin/node
'use strict';

// npm dependencies
const _      = require('lodash');
const cmdr   = require('commander');
const moment = require('moment');
const req_p  = require('request-promise-native');

// libraries
const database = require('./libs/database');
const logger   = require('./libs/logger');
const sla_api  = require('./libs/sla_api');

// get command line options
cmdr.version('1.0.0', '-v, --version')
.option('-f, --force', 'force update if caveats exist')
.option('--block <block time>', 'start from a particular block time')
.option('-p, --prod', 'run in the production environment')
.option('-d, --dev', 'run in the development environment')
.option('--env <environment>', 'specify the environment to run in')
.option('-r, --restart', 'run from the start block')
.parse(process.argv);

// get full config
const all_cfg = require('./config/config.json');

// determine environment
let env = process.env.NODE_ENV || 'development';

if (cmdr.dev) {
    env = 'development';
} else if (cmdr.env) {
    env = cmdr.env;
} else if (cmdr.prod) {
    env = 'production';
} else if (all_cfg.environment) {
    env = all_cfg.environment;
}

// get env config
const config = all_cfg[env];
const db_cfg = require('./config/db_config.json')[env];

if (!config || !db_cfg) {
    logger.error(`Environment: ${env} does not exist`);
    process.exit(1);
}

// initialize the database
const db  = database(db_cfg);
const sla = sla_api(config);

// options for requesting from crypto api
let crypto_opts = {
    method : 'GET',
    json   : true,

    httpSignature : {
        key   : config.crypto_api.priv_key,
        keyId : config.crypto_api.id
    },

    forever : true
};

// process caveat
let proc_fn = async (caveat, cntr) => {
    
    let lbt = caveat.last_block_time;
    let cvt = caveat.chain_doc;
    let key = caveat.strm_key;

    try {
        if (!cvt.transact_price || cvt.transact_price <= 0) {
            throw new Error('empty');
        }

        // skip processing error records
        if (cvt.address === '#NAME?') {
            throw new Error('#NAME?');
        }

        let mdl_cvt = await db.Caveat.findOne({where : {api_id : key}});

        // caveat exists in db but is latest version
        // only check if not being forced
        if (!cmdr.force) {
            if (mdl_cvt && moment(mdl_cvt.lst_blck_tm).isSameOrAfter(lbt)) {
                return logger.info(`skip ${cntr} key:${key} - ${mdl_cvt.address} ` + 
                `(${mdl_cvt.latitude}, ${mdl_cvt.longitude})`);
            }
        }
        
        let norm_addr = '';
        let proj_name = cvt.project_name === 'N.A.' ? '' : cvt.project_name;
        let pstl_code = cvt.postal_code.padStart(6, '0');
        let pstl_sect = cvt.postal_sect.padStart(2, '0');
        let pstl_dist = cvt.postal_dist.padStart(2, '0');
        
        // caveat create or update data
        let data = {
            api_id : key,
            
            project       : cvt.project_name,
            postal_code   : pstl_code,
            property_type : cvt.property_type,
            lst_blck_tm   : lbt,
            caveat_dt     : cvt.sale_dt,
            raw_project   : cvt.project_name,
            raw_address   : cvt.address,
            raw_sale_type : cvt.sale_type,
            raw_area_sqm  : cvt.area_sqm,
            raw_area_type : cvt.area_type,
            raw_plan_area : cvt.plan_area,
            
            raw_postal_code   : pstl_code,
            raw_property_type : cvt.property_type,
            raw_tenure_type   : cvt.tenure,
            raw_plan_region   : cvt.plan_regn,
            raw_postal_sector : pstl_sect,
            transact_price    : cvt.transact_price,
            last_block_time   : lbt,

            raw_postal_district : pstl_dist,
            transact_price_sqm  : cvt.unit_price_sqm,
            transact_price_sqf  : cvt.unit_price_sqf
        };

        // floor area or land area
        if (cvt.area_type === 'Strata') {
            data.floor_area_sqm = cvt.area_sqm;
            data.land_area_sqm  = 0;
            data.floor_area_sqf = cvt.area_sqm * 10.7639;
            data.land_area_sqf  = 0;
        } else {
            data.floor_area_sqm = 0;
            data.land_area_sqm  = cvt.area_sqm;
            data.floor_area_sqf = 0;
            data.land_area_sqf  = cvt.area_sqm * 10.7639;
        }

        // **** applies to residential caveats ****
        if (cvt.unit_cnt) {
            data.raw_unit_cnt = cvt.unit_cnt;
        }

        if (cvt.buyer_addr_type) {
            data.raw_buyer_address_type = cvt.buyer_addr_type;
        }

        if (cvt.complete_yr) {
            data.complete_yr = cvt.complete_yr;
        }

        if (cvt.net_price) {
            data.raw_net_price = cvt.net_price;
        }
        // **** end ****

        // process address
        let reg_exp = /((?:plot\s+[a-z0-9]+)|(?:[a-z0-9-/]+,?)+(?:\s?etc)?)?([^#]*)\s?(#.*)?/i;
        let address = cvt.address.replace(/,\s+/, ',').replace(/\/\s+/, '/');

        let [,hse_no, street, unit_no] = reg_exp.exec(address);

        let orig_hse_no = hse_no;

        street  = street.trim();
        unit_no = unit_no || '';

        if (hse_no.includes(',')) {
            [hse_no] = hse_no.split(',');
        } else if (hse_no.includes('/')) {
            [hse_no] = hse_no.split('/');
        }

        data.house_no = orig_hse_no;

        if (hse_no.trim() === '-') {
            let sla_res = await sla.get(pstl_code);

            if (sla_res.results.length > 0) {
                // 1st try of sla api
                let sla_rslt = sla_res.results[0];

                norm_addr = (`${sla_rslt.BLK_NO} ${sla_rslt.ROAD_NAME} ` + 
                `${unit_no} ${proj_name} SINGAPORE ${sla_rslt.POSTAL}`)
                .replace(/\s+/g, ' ').toUpperCase().trim();

                data = _.assign(data, {
                    house_no    : sla_rslt.BLK_NO,
                    latitude    : sla_rslt.LATITUDE,
                    longitude   : sla_rslt.LONGITUDE,
                    street_name : sla_rslt.ROAD_NAME.toUpperCase()
                });
            }
        } else {
            let sla_res = await sla.get(`${hse_no} ${street}`);

            if (sla_res.results.length > 0) {
                // 1st try of sla api
                let sla_rslt = sla_res.results[0];

                norm_addr = (`${orig_hse_no} ${sla_rslt.ROAD_NAME} ` + 
                `${unit_no} ${proj_name} SINGAPORE ${sla_rslt.POSTAL}`)
                .replace(/\s+/g, ' ').toUpperCase().trim();

                data = _.assign(data, {
                    latitude    : sla_rslt.LATITUDE,
                    longitude   : sla_rslt.LONGITUDE,
                    street_name : sla_rslt.ROAD_NAME.toUpperCase()
                });
            }
        }
        
        if (norm_addr === '') {
            // try again if no results from sla api
            let sla_res = await sla.get(street);

            if (sla_res.results.length > 0) {
                // 2nd try of sla api
                let sla_rslt = sla_res.results[0];
                
                norm_addr = (`${orig_hse_no} ${sla_rslt.ROAD_NAME} ` +
                `${unit_no} ${proj_name} SINGAPORE ${pstl_code}`)
                .replace(/\s+/g, ' ').toUpperCase().trim();

                data = _.assign(data, {
                    latitude    : sla_rslt.LATITUDE,
                    longitude   : sla_rslt.LONGITUDE,
                    street_name : sla_rslt.ROAD_NAME.toUpperCase()
                });
            } else {
                norm_addr = `${address} ${proj_name} SINGAPORE ${pstl_code}`
                .replace(/\s+/g, ' ').toUpperCase().trim();

                data = _.assign(data, {
                    latitude    : 1.290270,
                    longitude   : 103.851959,
                    street_name : street.toUpperCase()
                });
            }
        }

        data.address = norm_addr;
        data.unit_no = unit_no.replace(/#/, '');
        
        if (mdl_cvt) {
            mdl_cvt = await mdl_cvt.update(data);
        } else {
            mdl_cvt = await db.Caveat.create(data);
        }
        
        logger.info(`complete ${cntr} key:${key} - ${data.address} ` + 
        `(${data.latitude}, ${data.longitude})`);
    } catch (error) {
        logger.error(`${error.message} ${cntr} ${moment(lbt).utc().format()} ${key} \n` + 
        `${JSON.stringify(caveat, null, 2)}\n${error.stack}`);
    }
};

(async () => {
    try {

        let cntr = 0;
        let next = '/v1/stream/caveats/documents/seek?' + 
        'seek=last_block_time|asc&limit=100&seek_fwd=1';

        if (cmdr.block) {
            next += `&last_block_time=${moment(cmdr.block).utc().format()}|`;
        } else {
            if (!cmdr.restart) {
                let max_blck_tm = await db.Caveat.max('lst_blck_tm');

                if (max_blck_tm) {
                    next += `&last_block_time=${moment(max_blck_tm).utc().format()}|`;
                }
            }
        }

        while (next) {
            crypto_opts.uri = `${config.crypto_api.uri}${next}`;

            let crypto_res = await req_p(crypto_opts);
            
            let caveats  = crypto_res.resources;
            let iterates = [];
            
            for (let caveat of caveats) {
                iterates.push(proc_fn(caveat, ++cntr));

                if (cntr % 5 === 0) {
                    await Promise.all(iterates);
                    iterates = [];
                }
            }

            // leftover caveats
            if (iterates.length > 0) {
                await Promise.all(iterates);
            }

            next = crypto_res.meta.next;
        }
        logger.info('Process complete');
    } catch (error) {
        logger.error(`error:"${error.message}"\n${error.stack}`);
    }

    process.exit(0);
})();