#! /usr/local/bin/node
'use strict';

// npm dependencies
const _      = require('lodash');
const cmdr   = require('commander');
const moment = require('moment');
const req_p  = require('request-promise-native');

// libraries
const database = require('./libs/database');
const logger   = require('./libs/logger');
const sla_api  = require('./libs/sla_api');

// get command line options
cmdr.version('1.0.0', '-v, --version')
.option('-f, --force', 'force update of existing HDB sales')
.option('--block <block time>', 'start from a particular block time')
.option('-p, --prod', 'run in the production environment')
.option('-d, --dev', 'run in the development environment')
.option('--env <environment>', 'specify the environment to run in')
.option('-r, --restart', 'run from the start block')
.parse(process.argv);

// get full config
const all_cfg = require('./config/config.json');

// determine environment
let env = process.env.NODE_ENV || 'development';

if (cmdr.dev) {
    env = 'development';
} else if (cmdr.env) {
    env = cmdr.env;
} else if (cmdr.prod) {
    env = 'production';
} else if (all_cfg.environment) {
    env = all_cfg.environment;
}

// get env config
const config = all_cfg[env];
const db_cfg = require('./config/db_config.json')[env];

if (!config || !db_cfg) {
    logger.error(`Environment: ${env} does not exist`);
    process.exit(1);
}

// initialize the database
const db  = database(db_cfg);
const sla = sla_api(config);

// options for requesting from crypto api
let crypto_opts = {
    method : 'GET',
    json   : true,

    httpSignature : {
        key   : config.crypto_api.priv_key,
        keyId : config.crypto_api.id
    },

    forever : true
};

// process caveat
let proc_fn = async (sale, cntr) => {
    
    let lbt = sale.last_block_time;
    let sal = sale.chain_doc;
    let key = sale.strm_key;

    try {
        if (!sal.street || sal.street.trim() === '') {
            throw new Error('empty');
        }

        let mdl_sal = await db.HdbSale.findOne({where : {api_id : key}});

        // caveat exists in db but is latest version
        if (!cmdr.force) {
            if (mdl_sal && moment(mdl_sal.lst_blck_tm).isSameOrAfter(lbt)) {
                return logger.info(`skip ${cntr} key:${key} - ${mdl_sal.address} ` + 
                `(${mdl_sal.latitude}, ${mdl_sal.longitude})`);
            }
        }

        // removing weird backticks
        sal.street  = sal.street.replace(/`/,'\'');
        sal.address = sal.address.replace(/`/,'\'');

        // hda sale create or update data
        let data = {
            api_id : key,

            raw_street  : sal.street,
            raw_address : sal.address,
            sale_dt     : sal.sale_dt,
            register_dt : sal.register_dt,
            room_type   : sal.room_type,
            model       : sal.model,
            hdb_office  : sal.hdb_office,
            house_no    : sal.house_num,
            unit_no     : sal.unit_num,

            raw_house_num   : sal.house_num,
            valuation_dt    : sal.valuation_dt,
            lease_start_dt  : sal.lease_start_dt,
            floor_area_sqm  : sal.floor_area_sqm,
            floor_area_sqf  : sal.floor_area_sqm * 10.7639,
            lst_blck_tm     : lbt,
            last_block_time : lbt,

            raw_recess_area    : sal.recess_area,
            transact_price     : sal.transact_price,
            transact_price_sqm : sal.transact_price / sal.floor_area_sqm,
            transact_price_sqf : sal.transact_price / (sal.floor_area_sqm * 10.7639)
        };

        let sla_res  = await sla.get(`${sal.house_num} ${sal.street}`);
        let sla_data = null;
        
        if (sla_res.results.length > 0) {
            for (let sla_rslt of sla_res.results) {
                if (sla_rslt.BLK_NO === sal.house_num) {
                    
                    let address = (`${sal.house_num} ${sla_rslt.ROAD_NAME} ` +
                    `${sal.unit_num ? '#' + sal.unit_num : ''} SINGAPORE ${sla_rslt.POSTAL}`)
                    .replace(/\s+/g, ' ').toUpperCase().trim();
                    
                    sla_data = {
                        street_name : sla_rslt.ROAD_NAME,
                        postal_code : sla_rslt.POSTAL,
                        latitude    : sla_rslt.LATITUDE,
                        longitude   : sla_rslt.LONGITUDE,
                        address
                    };

                    break;
                }
            }
        }

        if (!sla_data) {
            sla_data = {
                street_name : sal.street,
                postal_code : 'NIL',
                latitude    : 1.290270,
                longitude   : 103.851959,
                address     : sal.address
            };
        }

        data = _.assign(data, sla_data);
        
        if (mdl_sal) {
            mdl_sal = await mdl_sal.update(data);
        } else {
            mdl_sal = await db.HdbSale.create(data);
        }

        logger.info(`complete ${cntr} key:${key} - ${data.address} ` + 
        `(${data.latitude}, ${data.longitude})`);
    } catch (error) {
        logger.error(`${error.message} ${cntr} ${moment(lbt).utc().format()} ${key} \n` + 
        `${JSON.stringify(sale, null, 2)}\n${error.stack}`);
    }
};

(async () => {
    try {

        let cntr = 0;
        let next = '/v1/stream/hdb-sales/documents/seek?' + 
        'seek=last_block_time|asc&limit=100&seek_fwd=1';

        if (cmdr.block) {
            next += `&last_block_time=${moment(cmdr.block).utc().format()}|`;
        } else {
            if (!cmdr.restart) {
                let max_blck_tm = await db.HdbSale.max('lst_blck_tm');

                if (max_blck_tm) {
                    next += `&last_block_time=${moment(max_blck_tm).utc().format()}|`;
                }
            }
        }

        while (next) {
            crypto_opts.uri = `${config.crypto_api.uri}${next}`;

            let crypto_res = await req_p(crypto_opts);
            
            let sales    = crypto_res.resources;
            let iterates = [];
            
            for (let sale of sales) {
                iterates.push(proc_fn(sale, ++cntr));

                if (cntr % 5 === 0) {
                    await Promise.all(iterates);
                    iterates = [];
                }
            }

            // leftover caveats
            if (iterates.length > 0) {
                await Promise.all(iterates);
            }

            next = crypto_res.meta.next;
        }
        logger.info('Process complete');
    } catch (error) {
        logger.error(`error:"${error.message}"\n${error.stack}`);
    }

    process.exit(0);
})();
