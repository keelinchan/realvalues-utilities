#! /usr/local/bin/node
'use strict';

// core dependencies
const fs   = require('fs');
const path = require('path');

// npm dependencies
const _      = require('lodash');
const S3     = require('aws-sdk/clients/s3');
const cmdr   = require('commander');
const moment = require('moment');
const shell  = require('shelljs');

// libraries
const logger = require('./libs/logger');

// get command line options
cmdr.version('1.0.0', '-v, --version')
.option('-p, --prod', 'run in the production environment')
.option('-d, --dev', 'run in the development environment')
.option('--env <environment>', 'specify the environment to run in')
.option('--file <file>', 'specify the backup file name')
.parse(process.argv);

// get full config
const all_cfg = require('./config/config.json');

// determine environment
let env = process.env.NODE_ENV || 'development';

if (cmdr.dev) {
    env = 'development';
} else if (cmdr.env) {
    env = cmdr.env;
} else if (cmdr.prod) {
    env = 'production';
} else if (all_cfg.environment) {
    env = all_cfg.environment;
}

// get env config
const config = all_cfg[env];
const db_cfg = require('./config/db_config.json')[env];

let file_nm = moment.utc().format('YYYYMMDDHHmmss') + 
    `_${(cmdr.file || db_cfg.database)}.sql.gz`;

let file_pth = path.join(__dirname, 'baks', file_nm);

logger.info(`Dumping ${db_cfg.database} to ${file_pth} ...`);

shell.exec(
    `mysqldump -u ${db_cfg.username} ` +
    `-p${db_cfg.password} -h ${db_cfg.host} ` + 
    `-P ${db_cfg.port} ${db_cfg.database} | gzip ` +
    `> ${file_pth}`
);

logger.info(`Uploading ${file_nm} ...`);

let file_strm = fs.createReadStream(file_pth);

// upload to AWS S3
let s3 = new S3({
    apiVersion : '2006-03-01',
    
    accessKeyId     : config.aws.key_id,
    secretAccessKey : config.aws.access_key,

    region : config.aws.region
});

(async () => {
    try {
        // upload file
        let upld_res = await s3
            .upload({
                ACL    : 'private',
                Bucket : config.aws.s3_bucket, 
                Body   : file_strm,
                Key    : `${config.aws.dir}/${file_nm}`
            }, 
            {
                partSize  : 10 * 1024 * 1024, 
                queueSize : 1
            })
            .promise();

        logger.info(`Backup stored at: ${upld_res.Location}`);

        // removing file
        shell.rm('-rf', file_pth);

        // list backups in cloud
        let list_res = await s3
            .listObjectsV2({
                Bucket : config.aws.s3_bucket,
                Prefix : config.aws.dir
            })
            .promise();

        // delete last key
        let max_baks = (config.aws.max_baks || 30);


        if (list_res.KeyCount > max_baks) {
            let del_baks = _.reverse(_.sortBy(list_res.Contents, ['Key']))
                .slice(max_baks);

            for (let del_bak of del_baks) {
                if (del_bak.Key !== `${config.aws.dir}/`) {
                    logger.info(`Deleting ${del_bak.Key} ...`);

                    await s3
                        .deleteObject({
                            Bucket : config.aws.s3_bucket,
                            Key    : del_bak.Key
                        })
                        .promise();
                }
            }
        }

    } catch (error) {
        logger.error(error.message);
    }
})();