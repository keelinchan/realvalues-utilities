'use strict';

// core dependencies
const qry_str = require('querystring');

// npm dependencies
const req_p = require('request-promise-native');

class SlaApi {

    /**
     * @constructor
     *
     * @param {object} config
     * @param {Logger} logger
     */
    constructor (config, logger) {
        // configuration
        this.cfg  = config.sla_api || config;
        this.uri  = this.cfg.uri;
        this.opts = {
            method  : 'GET',
            json    : true,
            forever : true
        };
        this.retrys = this.cfg.retrys || [100, 200, 400];

        // logger
        this.logger = logger;
    }

    /**
     * call the sla api
     *
     * @param {string} search
     * @param {array} retrys
     * 
     * @return {Promise.<object, Error>}
     */
    async get (search, retrys) {
        // defaults
        retrys = retrys || this.retrys;

        this.opts.uri = this.uri + qry_str.stringify({searchVal : search});

        try {
            let res = await req_p(this.opts);

            if (res) {
                return res;
            } else {
                throw new Error('no_response');
            }
        } catch (error) {

            let retry = retrys.shift();
            
            if (retry) {
                // retry
                this.logger.info(`sla retry:${retry}ms error:${error.message}`);
                
                // wait
                await new Promise((res) => setTimeout(res, retry));
                
                return this.get(search, retrys);
            } else {
                throw error;
            }
        }
    }
}

module.exports = function (config, logger) {
    return new SlaApi(config, logger);
};