'use strict';

const cls     = require('cls-hooked');
const cls_b   = require('cls-bluebird');
const sqlz    = require('sequelize');
const Promise = require('bluebird');
const retry   = require('retry-as-promised');

// database connections
class Database {
    
    /**
     * constructor to setup the database
     */
    constructor (db_config) {
        // set sequelize object
        this.seq = sqlz;

        // create namspace
        this.nm_spc = cls.createNamespace('realvalues_sync');
        
        // patch bluebird promises
        cls_b(this.nm_spc, Promise);

        // include cls in sequelize
        this.seq.useCLS(this.nm_spc);

        // sequelize connection string
        let connect_str = `${db_config.dialect}://${db_config.username}:${db_config.password}@` + 
        `${db_config.host}:${db_config.port}/${db_config.database}`;

        this.db = new this.seq(connect_str, {
            dialectOptions : {decimalNumbers : true},
            isolationLevel : this.seq.Transaction.ISOLATION_LEVELS.READ_COMMITTED,

            logging : false
        });

        // models container
        this.models = {
            Caveat  : this.caveats(),
            HdbSale : this.hdb_sales()
        };

        // convenience mapping
        this.Caveat  = this.models.Caveat;
        this.HdbSale = this.models.HdbSale;

        //this.db.sync();
    }

    /**
     * database transaction
     */
    trxn (fn) {
        let seq = this.seq;

        return retry(() => {
            return this.db.transaction(fn);
        }, {
            max     : 3,
            timeout : 10000,
            match   : [
                seq.ConnectionError,
                seq.ConnectionRefusedError,
                seq.ConnectionTimedOutError,
                seq.OptimisticLockError,
                seq.TimeoutError,
                /Deadlock/i
            ],
            
            backoffBase     : 1000,
            backoffExponent : 1.5
        });
    }

    /**
     * initialize caveats 
     */
    caveats () {
        let db  = this.db;
        let seq = this.seq;

        return db.define('caveats', {
            id : {
                type          : seq.INTEGER(10).UNSIGNED,
                primaryKey    : true,
                autoIncrement : true
            },

            api_id : seq.STRING,
            
            raw_project            : seq.STRING,
            raw_address            : seq.STRING,
            raw_postal_code        : seq.STRING(20),
            raw_postal_sector      : seq.STRING(2),
            raw_postal_district    : seq.STRING,
            raw_unit_cnt           : seq.STRING,
            raw_area_sqm           : seq.STRING,
            raw_area_type          : seq.STRING,
            raw_net_price          : seq.DECIMAL(13,2),
            raw_property_type      : seq.STRING,
            raw_tenure_type        : seq.STRING,
            raw_sale_type          : seq.STRING,
            raw_buyer_address_type : seq.STRING,
            raw_plan_region        : seq.STRING,
            raw_plan_area          : seq.STRING,

            address_id    : seq.UUID,
            caveat_dt     : seq.DATEONLY,
            property_type : seq.STRING,
            tenure_type   : seq.STRING,
            other_tenure  : seq.STRING,
            address       : seq.STRING,
            house_no      : seq.STRING,
            street_name   : seq.STRING,
            unit_no       : seq.STRING,
            project       : seq.STRING,
            postal_code   : seq.STRING,
            complete_yr   : seq.STRING,
            
            last_block_time : seq.DATE,
            lst_blck_tm     : seq.DATE, 

            floor_area_sqm     : seq.DECIMAL(13,2),
            floor_area_sqf     : seq.DECIMAL(13,2),
            land_area_sqm      : seq.DECIMAL(13,2),
            land_area_sqf      : seq.DECIMAL(13,2),
            transact_price     : seq.DECIMAL(13,2),
            transact_price_sqm : seq.DECIMAL(13,2),
            transact_price_sqf : seq.DECIMAL(13,2),

            latitude  : seq.DOUBLE,
            longitude : seq.DOUBLE
        }, {
            indexes : [
                {fields : ['api_id']},
                {fields : ['lst_blck_tm']}
            ],
            
            timestamps : false
        });
    }

    /**
     * initialize hdb_sales
     */
    hdb_sales () {
        let db  = this.db;
        let seq = this.seq;

        return db.define('hdb_sales', {
            id : {
                type          : seq.INTEGER(10).UNSIGNED,
                primaryKey    : true,
                autoIncrement : true
            },

            api_id : seq.STRING,

            address_id  : seq.UUID,
            raw_street  : seq.STRING,
            raw_address : seq.STRING,
            sale_dt     : seq.DATEONLY,
            register_dt : seq.DATEONLY,

            raw_house_num  : seq.STRING,
            valuation_dt   : seq.DATEONLY,
            lease_start_dt : seq.DATEONLY,
            top_dt         : seq.DATEONLY,
            floor_area_sqm : seq.DECIMAL(13,2),
            floor_area_sqf : seq.DECIMAL(13,2),
            
            raw_recess_area    : seq.STRING,
            recess_area_type   : seq.STRING,
            transact_price     : seq.DECIMAL(13,2),
            transact_price_sqm : seq.DECIMAL(13,2),
            transact_price_sqf : seq.DECIMAL(13,2),

            room_type   : seq.STRING(10),
            model       : seq.STRING,
            hdb_office  : seq.STRING,
            address     : seq.STRING,
            house_no    : seq.STRING,
            street_name : seq.STRING,
            unit_no     : seq.STRING,
            project     : seq.STRING,
            postal_code : seq.STRING,
            
            latitude  : seq.DOUBLE,
            longitude : seq.DOUBLE,
            
            last_block_time : seq.DATE,
            lst_blck_tm     : seq.DATE 
        }, {
            indexes : [
                {fields : ['api_id']},
                {fields : ['lst_blck_tm']}
            ],

            timestamps : false
        });
    }
}

module.exports = function (db_config) {
    return new Database(db_config);
};