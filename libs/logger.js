'use strict';

const winston = require('winston');

const logger = new winston.createLogger({
    transports : [
        new winston.transports.Console({
            format : winston.format.combine(
                winston.format.timestamp({
                    format : 'YYYYMMDD HH:mm:ssZ'
                }),

                winston.format.printf((info) => {
                    return `${info.timestamp} ${info.level}: ${info.message}`;
                })
            )
        })
    ],

    levels : {
        error : 0,
        info  : 2
    }
});

module.exports = logger;